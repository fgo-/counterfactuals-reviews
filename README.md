# Counterfactuals using DiCE on text data.

This is an example work to investigate whether [DiCE](https://www.microsoft.com/en-us/research/project/dice/#!publications) could be used with text data, specifically on a sentiment analysis problem on movie reviews.

## Preparation

After cloning the repository,  install the requirements:

```shell
pip install -r requirements.txt
```

Download and extract the movie reviews dataset into a `data/` folder:

```shell
mkdir data && cd data/
wget https://raw.githubusercontent.com/jbrownlee/Datasets/master/review_polarity.tar.gz
tar xzf review_polarity.tar.gz
cd ..
```

## Run the notebook

Nothing more to be said ;)

I intentionally saved the notebook with the outputs, so that you have an idea of what to expect in terms of results and computation time (it was run on a laptop).

Notice that this is (obviously) not a production code.


## Credits

### Dataset

``A Sentimental Education: Sentiment Analysis Using Subjectivity Summarization Based on Minimum Cuts``, Proceedings of the ACL, 2004.

### Code

Model and preprocessing adapted from:

Jason Brownlee, [``How to Develop a Deep Learning Bag-of-Words Model for Sentiment Analysis (Text Classification)``](https://machinelearningmastery.com/deep-learning-bag-of-words-model-sentiment-analysis/), Machine Learning Mastery, 2016. 
Available from https://machinelearningmastery.com/deep-learning-bag-of-words-model-sentiment-analysis/, accessed June 28th, 2021.
